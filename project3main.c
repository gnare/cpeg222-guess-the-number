/* ************************************************************************** */
/**

  @Author
    Galen Nare

  @File Name
    project3main.c

  @Summary
    Main source file and control flow for CPEG 222 Guess the number project.
 */
/* ************************************************************************** */

#include "common.h"

uint32_t intbuf = 0;

unsigned int mode = 1;
unsigned int n_players = 0;

volatile unsigned int active_player = 0;
volatile unsigned int time_left = 20.0;

int number = 255;
int lowest = 0;
int highest = 99;

int entry_buffer = 0;

int LD_position = 0;

int main() {    
    DDPCONbits.JTAGEN = 0; // Statement is required to use Pin RA0 as IO
    TRISA &= 0xFF00; // Set Port A bits 0~7 to 0, i.e., LD 0~7 are configured as digital output pins
    LATA = LD_position; // write the hex value of LD_position to the 8 LEDs
    
    TRISDbits.TRISD0 = 0; // Set PMOD port states
    TRISDbits.TRISD1 = 0;
    ANSELDbits.ANSD1 = 0;
    
    TRISCbits.TRISC14 = 0;
    TRISCbits.TRISC13 = 0;
    TRISDbits.TRISD8 = 1;
    TRISDbits.TRISD9 = 1;
    TRISDbits.TRISD10 = 1;
    TRISDbits.TRISD11 = 1;
    
    ACL_Init();
    ACL_SetRange(0);
    SSD_Init();
    LCD_Init(); // Init and clear the drivers for the LCD and SSD.
    
    CN_config();
    Timer2_Setup();
    
    ssd_digit_mask(0);
    
    lcd_display_start();
    
    int seed = 0;
    
    while (1) {
        delay_ms(10);
        /*char buf[10];
        sprintf(buf, "%d", SSD_GetDigitsPTR()[0]);
        lcd_print(buf);*/
        
        if (mode < 3) {
            ssd_digit_mask(0); // All SSD digits should be off unless it's mode 3 or 4.
        }
    }
}

void led_active_player(int player) {
    if (player > 0) {
        LD_position = 1 << (player - 1);
        LATA = LD_position;
    } else {
        LD_position = 0;
        LATA = LD_position;
    }
}

/* ----------------------------------------------------------------------------- 
 **	lcd_print
 **	Parameters:
 **		str - the string to print
 **	Return Value:
 **		nothing
 **	Description:
 **		Print a string to the LCD at pos 0
 ** -------------------------------------------------------------------------- */
void lcd_print(char* str) {
    delay_ms(5);
    LCD_WriteStringAtPos(str, 0, 0);
}

/* ----------------------------------------------------------------------------- 
 **	lcd_display_start
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		Print the mode 1 display to the LCD
 ** -------------------------------------------------------------------------- */
void lcd_display_start() {
    delay_ms(5);
    LCD_WriteStringAtPos("Number Guessing ", 0, 0);
    LCD_WriteStringAtPos("# players?      ", 1, 0);
}

/* ----------------------------------------------------------------------------- 
 **	lcd_display_mode2
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		Displays guess mode 2 text on the LCD.
 ** -------------------------------------------------------------------------- */
void lcd_display_mode2() {
    delay_ms(5);
    char buffer[20];
    sprintf(buffer, "# Guessing -- %d ", n_players);
    LCD_WriteStringAtPos(buffer, 0, 0);
    LCD_WriteStringAtPos("Rand(E), Det(D)?", 1, 0);
}

/* ----------------------------------------------------------------------------- 
 **	lcd_display_mode3
 **	Parameters:
 **		deterministic - 0 = Random secret, 1 = deterministic
 **	Return Value:
 **		nothing
 **	Description:
 **		Displays mode 3 text on the LCD.
 ** -------------------------------------------------------------------------- */
void lcd_display_mode3(int deterministic) {
    delay_ms(5);
    char buffer[20];
    sprintf(buffer, "# Guessing -- %d ", n_players);
    LCD_WriteStringAtPos(buffer, 0, 0);
    if (deterministic == 1) {
        LCD_WriteStringAtPos("Deterministic   ", 1, 0);
    } else {
        LCD_WriteStringAtPos("Random Secret   ", 1, 0);
    }
}

/* ----------------------------------------------------------------------------- 
 **	lcd_display_feedback
 **	Parameters:
 **		mode - 0 = You got it, 1 = Too low, 2 = too high
 **	Return Value:
 **		nothing
 **	Description:
 **		Displays guess feedback on the LCD.
 ** -------------------------------------------------------------------------- */
void lcd_display_feedback(int mode) {
    delay_ms(5);
    switch (mode) {
        case 0:
            LCD_WriteStringAtPos("You got it!     ", 1, 0);
            return;
        case 1:
            LCD_WriteStringAtPos("Too low         ", 1, 0);
            return;
        case 2:
            LCD_WriteStringAtPos("Too high        ", 1, 0);
            return;
    }
}

/* ----------------------------------------------------------------------------- 
 **	ssd_digit_mask
 **	Parameters:
 **		mask - a bitmask to select SSD digits to be enabled/disabled.
 **	Return Value:
 **		nothing
 **	Description:
 **		Turns on/off SSD digits based on the bitmask. Requires modified ssd.c/.h
 **     file.
 ** -------------------------------------------------------------------------- */
void ssd_digit_mask(uint8_t mask) {
    unsigned char* digits = SSD_GetDigitsPTR(); // We directly edit the digits array on ssd.c so we can turn it off
    digits[0] |= (mask % 2 == 0) ? 255 : 0;
    digits[1] |= ((mask >> 1) % 2 == 0) ? 255 : 0;
    digits[2] |= ((mask >> 2) % 2 == 0) ? 255 : 0;
    digits[3] |= ((mask >> 3) % 2 == 0) ? 255 : 0;
}

/* ----------------------------------------------------------------------------- 
 **	acl_srand
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		Seed the random number generator with noise from the accelerometer
 **     ACL must already have been initialized.
 ** -------------------------------------------------------------------------- */
int acl_srand() {
    float rgACLGVals[3];
    ACL_ReadGValues(rgACLGVals); // Read the accelerometer.
    int* x_ptr = (int*) &(rgACLGVals[0]); // Use the bits of the float as an integer, we don't care about the actual value
    srand(*x_ptr); // Seed the RNG
    return *x_ptr; // For debugging
}

/* ----------------------------------------------------------------------------- 
 **	ssd_write_hex
 **	Parameters:
 **		n1 - the number (up to 2 digits) to write to the left SSD
 **     n2 - the number (up to 2 digits) to write to the right SSD
 **	Return Value:
 **		nothing
 **	Description:
 **		Displays 2 integers on the SSD. Truncates all values to a maximum value
 **     of 99. No negative values.
 ** -------------------------------------------------------------------------- */
void ssd_write_ints(uint8_t n1, uint8_t n2) {
    uint8_t ssd1_tens = n1 / 10, ssd1_ones = n1 % 10; // Split the numbers for the SSD.
    uint8_t ssd2_tens = n2 / 10, ssd2_ones = n2 % 10;

    SSD_WriteDigits(ssd2_ones, ssd2_tens, ssd1_ones, ssd1_tens,
        0, 0, 0, 0); // Write the numbers to the SSD
    delay_ms(20);
}


/* ----------------------------------------------------------------------------- 
 **	ssd_write_hex
 **	Parameters:
 **		hex - the hex digits to display on the SSD
 **	Return Value:
 **		nothing
 **	Description:
 **		Displays a raw hex value on the SSD. Can use all 4 digits.
 **     This is used for debugging.
 ** -------------------------------------------------------------------------- */
void ssd_write_hex(uint_least32_t hex) {
    SSD_WriteDigits(hex % 16, (hex >> 8) % 16, (hex >> 16) % 16, hex >> 24,
            0, 0, 0, 0); // Map hex digits to SSD
}


/* ----------------------------------------------------------------------------- 
 **	CN_config
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		Setup CN interrupts
 ** -------------------------------------------------------------------------- */
void CN_config() {
    macro_disable_interrupts;
    
    CNCONDbits.ON = 1;
    CNEND = 0xf00;
    CNPUD = 0xf00;
    
    PORTD;
    IFS1bits.CNDIF = 0;
    IEC1bits.CNDIE = 1;
    IPC8bits.CNIP = 6;
    IPC8bits.CNIS = 3;
    
    macro_enable_interrupts();
}


/* ----------------------------------------------------------------------------- 
 **	Timer2ISR
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		ISR handler for Timer2. Tracks how much time players have left to guess.
 ** -------------------------------------------------------------------------- */
void __ISR(_TIMER_2_VECTOR) Timer2ISR() {
    IFS0bits.T2IF = 0;
    T2CONbits.ON = 0; 
    if (mode < 3) {
        //return;
    } else {
        if (time_left > 0) {
            time_left -= 1;
            if (mode == 3) {
                ssd_write_ints(time_left, entry_buffer);
            }
        }

        if (time_left <= 0) {
            entry_buffer = 0;
            if (active_player < n_players) {
                active_player += 1;
            } else {
                active_player = 1;
            }
            mode = 3;
            led_active_player(active_player);
            time_left = 20;
            ssd_write_ints(time_left, 0);
        }
    }
    T2CONbits.ON = 1; 
}

/* ----------------------------------------------------------------------------- 
 **	CN_handler
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		Handles Change Notice Interrupt events.
 ** -------------------------------------------------------------------------- */
void __ISR(_CHANGE_NOTICE_VECTOR) CN_handler() {
    IEC1bits.CNDIE = 0; // Disable interrupt
    
    // Decode button press
    int btn = -1;
    R1 = 0;
    R2 = R3 = R4 = 1;
    if (C1 == 0) btn = 0x1;
    else if (C2 == 0) btn = 0x2;
    else if (C3 == 0) btn = 0x3;
    else if (C4 == 0) btn = 0xa;
    
    R2 = 0;
    R1 = R3 = R4 = 1;
    if (C1 == 0) btn = 0x4;
    else if (C2 == 0) btn = 0x5;
    else if (C3 == 0) btn = 0x6;
    else if (C4 == 0) btn = 0xb;
    
    R3 = 0;
    R1 = R2 = R4 = 1;
    if (C1 == 0) btn = 0x7;
    else if (C2 == 0) btn = 0x8;
    else if (C3 == 0) btn = 0x9;
    else if (C4 == 0) btn = 0xc;
    
    R4 = 0;
    R1 = R2 = R3 = 1;
    if (C1 == 0) btn = 0x0;
    else if (C2 == 0) btn = 0xf;
    else if (C3 == 0) btn = 0xe;
    else if (C4 == 0) btn = 0xd;
    
    R1 = R2 = R3 = R4 = 0; // Clear all rows
    
    delay_ms(30); // Debounce
    if (btn != -1) {
        CN_button_press(btn); // Pass decoded button press to handler
    }
    
    PORTD; // Wipe port
    IFS1bits.CNDIF = 0; // Clear interrupt flag
    IEC1bits.CNDIE = 1; // Re-enable interrupt
    
    //LATA = ~LATA;
}

/* ----------------------------------------------------------------------------- 
 **	CN_button_press
 **	Parameters:
 **		button - the button pressed
 **	Return Value:
 **		none
 **	Description:
 **		Handle button presses based on the software mode.
 ** -------------------------------------------------------------------------- */
void CN_button_press(unsigned int button) {
    switch (mode) {
        case 1:
            if (button <= 4 && button > 0) {
                n_players = button;
                mode = 2;
                lcd_display_mode2();
            }
            
            return;
        case 2:
            if (button == 0xe) {
                acl_srand();
                number = rand() % 100;
                mode = 3;
                active_player = 1;
                led_active_player(active_player);
                lcd_display_mode3(0);
                ssd_write_ints(time_left, 0);
                T2CONbits.ON = 1;
            } else if (button == 0xd) {
                number = DET_NUMBER;
                mode = 3;
                active_player = 1;
                led_active_player(active_player);
                lcd_display_mode3(1);
                ssd_write_ints(time_left, 0);
                T2CONbits.ON = 1;
            }
            return;
        case 3:
            if (button >= 0 && button <= 9) {
                if (entry_buffer > 0) {
                    entry_buffer *= 10;
                    entry_buffer += button;
                    entry_buffer %= 100; // Prevent overflow
                    ssd_write_ints(time_left, entry_buffer);
                } else {
                    entry_buffer = button;
                    ssd_write_ints(time_left, entry_buffer);
                }
            } else if (button == 0xe) {
                if (entry_buffer == number) {
                    lcd_display_feedback(0);
                    mode = 5;
                } else if (entry_buffer < number) {
                    if (entry_buffer < lowest) {
                        return;
                    } else {
                        lowest = entry_buffer;
                    }
                    lcd_display_feedback(1);
                    entry_buffer = 0;
                    if (active_player < n_players) {
                        active_player += 1;
                    } else {
                        active_player = 1;
                    }
                    led_active_player(active_player);
                    ssd_write_ints(time_left, 0);
                    time_left = 20;
                } else if (entry_buffer > number) {
                    if (entry_buffer > highest) {
                        return;
                    } else {
                        highest = entry_buffer;
                    }
                    lcd_display_feedback(2);
                    entry_buffer = 0;
                    if (active_player < n_players) {
                        active_player += 1;
                    } else {
                        active_player = 1;
                    }
                    led_active_player(active_player);
                    ssd_write_ints(time_left, 0);
                    time_left = 20;
                }
            } else if (button == 0xc) {
                entry_buffer = 0;
                ssd_write_ints(time_left, entry_buffer);
            } else if (button == 0xd) {
                entry_buffer /= 10;
                ssd_write_ints(time_left, entry_buffer);
            } else if (button == 0xf) {
                mode = 4;
                ssd_write_ints(lowest, highest);
            }
            
            return;
        case 4:
            if (button == 0xf) {
                mode = 3;
                ssd_write_ints(time_left, entry_buffer);
            }
            return;
        case 5:
            mode = 1;
            n_players = 0;

            active_player = 0;
            time_left = 20.0;

            number = 255;
            lowest = 0;
            highest = 99;

            entry_buffer = 0;

            LD_position = 0;
            LATA = LD_position;
            
            ssd_digit_mask(0);
    
            lcd_display_start();
            
            T2CONbits.ON = 0; 
            return;
    }
}

/* ----------------------------------------------------------------------------- 
 **	Timer2_Setup
 **	Parameters:
 **		none
 **	Return Value:
 **		nothing
 **	Description:
 **		Setup Timer2 interrupts
 ** -------------------------------------------------------------------------- */
void Timer2_Setup() {
    PR2 = 19530 * 2; // 1 second instead of 500ms
    TMR2 = 0;
    T2CONbits.TCKPS = 7; // 1:256 divider
    T2CONbits.TGATE = 0;
    T2CONbits.TCS = 0;
    T2CONbits.ON = 0; // Wait until mode 3 to enable
    
    IPC2bits.T2IP = 3; // Don't conflict with SSD timer, this function can wait if necessary 
    IPC2bits.T2IS = 3;
    IFS0bits.T2IF = 0;
    IEC0bits.T2IE = 1;
    macro_enable_interrupts(); // Commit the interrupt changes
}

/* ----------------------------------------------------------------------------- 
 **	delay_ms
 **	Parameters:
 **		ms - amount of milliseconds to delay (based on 80 MHz SSCLK)
 **	Return Value:
 **		none
 **	Description:
 **		Create a delay by counting up to counter variable
 ** -------------------------------------------------------------------------- */
void delay_ms(int ms) {
    int i, counter;
    for (counter = 0; counter < ms; counter++) {
        for (i = 0; i < MS_CONSTANT; i++) {
        } //software delay 1 milliseconds
    }
}